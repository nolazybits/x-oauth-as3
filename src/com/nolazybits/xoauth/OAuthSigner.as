/**
 * User: xavier
 * Date: 7/27/12
 * Time: 11:21 AM
 */
package com.nolazybits.xoauth
{
    import com.nolazybits.xoauth.signaturesMethods.IOAuthSignatureMethod;
    import com.nolazybits.xoauth.signaturesMethods.OAuthSignatureMethod_HMAC_SHA1;
    import com.nolazybits.xoauth.utils.URLEncoder;

    import flash.net.URLRequest;
    import flash.net.URLRequestHeader;

    public class OAuthSigner
    {
        /**
         * Consumer Key
         */
        protected var _consumerKey          : String;

        /**
         * Consumer Secret
         */
        protected var _consumerSecret       : String;


        public function OAuthSigner( consumerKey : String,  consumerSecret : String ) : void
        {
            if( consumerKey == "" )
            {
                throw new Error("Consumer key must not be empty/null");
            }

            if (consumerSecret == "")
            {
                throw new Error("Consumer secret must not be empty/null");
            }

            _consumerKey = consumerKey;
            _consumerSecret = consumerSecret;
        }

        /**
         * Sign the request when requesting a request token
         * @param urlRequest
         * @param urlCallback
         * @param signatureMethod
         */
        public function signForRequestToken(urlRequest : URLRequest, urlCallback : String, signatureMethod : IOAuthSignatureMethod = null) : void
        {
            if( signatureMethod === null)
            {
                signatureMethod = new OAuthSignatureMethod_HMAC_SHA1();
            }

            var oauthParameters : OAuthParameters= new OAuthParameters(_consumerKey, signatureMethod);
            oauthParameters.addParameters(urlRequest.data);
            oauthParameters.addParameters(_getGETParameters(urlRequest));
            oauthParameters.add(OAuthConstants.CALLBACK, urlCallback);

            var signature : String = getSignature(urlRequest, oauthParameters, OAuthConstants.EMPTY_TOKEN_SECRET, signatureMethod);
            oauthParameters.add(OAuthConstants.SIGNATURE, signature);
            //
            var headerString : String = oauthParameters.getAuthorizationHeaderValue();
            urlRequest.requestHeaders.push(OAuthConstants.HEADER, headerString);
        }

        /**
         * Sign the request when requesting an access token
         * @param urlRequest
         * @param token
         * @param tokenSecret
         * @param signatureMethod
         * @param verifier
         */
        public function signForAccessToken(urlRequest : URLRequest, token : String, tokenSecret : String, signatureMethod : IOAuthSignatureMethod = null, verifier : String = '') : void
        {
            if( signatureMethod === null)
            {
                signatureMethod = new OAuthSignatureMethod_HMAC_SHA1();
            }

            var oauthParameters : OAuthParameters= new OAuthParameters(_consumerKey, signatureMethod);
            oauthParameters.addParameters(urlRequest.data);
            oauthParameters.addParameters(_getGETParameters(urlRequest));
            oauthParameters.add(OAuthConstants.TOKEN, token);
            oauthParameters.add(OAuthConstants.VERIFIER, verifier);

            var signature : String = getSignature(urlRequest, oauthParameters, tokenSecret, signatureMethod);
            oauthParameters.add(OAuthConstants.SIGNATURE, signature);

            var headerString : String = oauthParameters.getAuthorizationHeaderValue();
            urlRequest.requestHeaders.push(OAuthConstants.HEADER, headerString);
        }

        /**
         * Sign the request. For tokenless request just pass the urlRequest.
         * @param urlRequest
         * @param token
         * @param tokenSecret
         * @param signatureMethod
         */
        public function sign( urlRequest : URLRequest, token : String = "", tokenSecret : String = "", signatureMethod : IOAuthSignatureMethod = null) : void
        {
            if( signatureMethod === null)
            {
                signatureMethod = new OAuthSignatureMethod_HMAC_SHA1();
            }

            //  create oauth parameters
            var oauthParameters : OAuthParameters = new OAuthParameters(_consumerKey, signatureMethod);
            oauthParameters.addParameters(urlRequest.data);
            oauthParameters.addParameters(_getGETParameters(urlRequest));

            //  add the token to the header if any
            if( token && token !== "" )
            {
                oauthParameters.add(OAuthConstants.TOKEN, token);
            }

            //  get the signature
            var signature : String = getSignature(urlRequest, oauthParameters, tokenSecret, signatureMethod);
            oauthParameters.add(OAuthConstants.SIGNATURE, signature);

            //  add the oauth header
            var headerString : String = oauthParameters.getAuthorizationHeaderValue();
            var urlRequestHeader : URLRequestHeader = new URLRequestHeader(OAuthConstants.HEADER, headerString);
            urlRequest.requestHeaders.push(urlRequestHeader);
        }

        /**
         * Generate a signature from the given base string, consumer key and token secret
         * @param baseString
         * @param consumerKey
         * @param tokenSecret
         */
        protected function getSignature(urlRequest : URLRequest, oauthParameters : OAuthParameters, tokenSecret : String, signatureMethod : IOAuthSignatureMethod = null) : String
        {
            if( signatureMethod === null)
            {
                signatureMethod = new OAuthSignatureMethod_HMAC_SHA1();
            }
            return signatureMethod.signRequest(urlRequest, this, oauthParameters, tokenSecret);
        }

        protected function _getGETParameters(urlRequest : URLRequest) : Array
        {
            var getParameters : Array = [];
            var indexQuestionMark : int = urlRequest.url.indexOf('?');
            if (indexQuestionMark == -1)
            {
                return getParameters;
            }

            var paramString : String = urlRequest.url.slice(urlRequest.url.indexOf('?')+1);
            var params : Array = paramString.split('&');
            var param : String;
            var keyValuePair : Array;
            for each (param in params)
            {
                keyValuePair = param.split("=");
                getParameters[keyValuePair[0]] = keyValuePair[1];
            }
            return getParameters;
        }

        public function getSignatureBaseString( urlRequest : URLRequest, oauthParameters : OAuthParameters) : String
        {
            var indexQuestionMark : int = urlRequest.url.indexOf('?');
            // create the string to be signed
            var ret:String = URLEncoder.encode(urlRequest.method.toUpperCase());
            ret += "&";
            ret += URLEncoder.encode( indexQuestionMark==-1?urlRequest.url:urlRequest.url.slice( 0, indexQuestionMark ) );
            ret += "&";
            ret += URLEncoder.encode( oauthParameters.getSortedEncodedParametersAsString() );
            return ret;
        }

        public function get consumerKey() : String
        {
            return _consumerKey;
        }

        public function get consumerSecret() : String
        {
            return _consumerSecret;
        }
    }
}
