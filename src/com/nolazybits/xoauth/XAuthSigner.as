/**
 * User: xavier
 * Date: 7/30/12
 * Time: 3:51 PM
 */
package com.nolazybits.xoauth
{
    import com.nolazybits.xoauth.signaturesMethods.IOAuthSignatureMethod;
    import com.nolazybits.xoauth.signaturesMethods.OAuthSignatureMethod_HMAC_SHA1;

    import flash.net.URLRequest;
    import flash.net.URLRequestHeader;
    import flash.net.URLVariables;
    import flash.net.URLVariables;

    public class XAuthSigner
        extends OAuthSigner
    {
        public function XAuthSigner( consumerKey : String, consumerSecret : String )
        {
            super(consumerKey, consumerSecret);
        }

        /**
         * <p>
         * Sign the given request to obtain the access token.
         * </p>
         * @param httpPost Http request.
         * @param username User name.
         * @param password Password.
         */
        public function signForXAuthAccessToken(
                urlRequest : URLRequest,
                username : String,
                password : String,
                signatureMethod : IOAuthSignatureMethod = null
                ) : void
        {
            if( signatureMethod === null)
            {
                signatureMethod = new OAuthSignatureMethod_HMAC_SHA1();
            }

            //  get the body parameters
            var parameters : URLVariables = urlRequest.data as URLVariables;
            parameters[OAuthConstants.MODE] = "client_auth";
            parameters[OAuthConstants.USERNAME] = username;
            parameters[OAuthConstants.PASSWORD] = password;

            var oauthParameters : OAuthParameters= new OAuthParameters(_consumerKey, signatureMethod);
            oauthParameters.addParameters(urlRequest.data);
            oauthParameters.addParameters(_getGETParameters(urlRequest));

            //  add the signature to the parameters
            var signature : String = getSignature( urlRequest, oauthParameters, OAuthConstants.EMPTY_TOKEN_SECRET, signatureMethod);
            oauthParameters.add(OAuthConstants.SIGNATURE, signature);

            //  set the authorization header
            var headerString : String = oauthParameters.getAuthorizationHeaderValue();
            var urlRequestHeader : URLRequestHeader = new URLRequestHeader(OAuthConstants.HEADER, headerString);
            urlRequest.requestHeaders.push(urlRequestHeader);
        }
    }
}
