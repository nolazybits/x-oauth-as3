/**
 * User: xavier
 * Date: 7/27/12
 * Time: 11:30 AM
 */
package com.nolazybits.xoauth.signaturesMethods
{
    import com.nolazybits.xoauth.OAuthParameters;
    import com.nolazybits.xoauth.OAuthSigner;

    import flash.net.URLRequest;

    public interface IOAuthSignatureMethod
    {
        function IOAuthSignatureMethod();

        function signRequest( urlRequest : URLRequest, signer : OAuthSigner, oauthParameters : OAuthParameters, tokenSecret : String = null) : String;
        function get name() : String;
    }
}
