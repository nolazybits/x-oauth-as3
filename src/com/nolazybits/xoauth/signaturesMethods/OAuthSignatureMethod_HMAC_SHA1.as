/**
 * User: xavier
 * Date: 7/30/12
 * Time: 1:20 PM
 */
package com.nolazybits.xoauth.signaturesMethods
{
    import com.hurlant.crypto.Crypto;
    import com.hurlant.crypto.hash.HMAC;
    import com.hurlant.util.Base64;
    import com.hurlant.util.Hex;
    import com.nolazybits.xoauth.OAuthConstants;
    import com.nolazybits.xoauth.OAuthParameters;
    import com.nolazybits.xoauth.OAuthSigner;
    import com.nolazybits.xoauth.utils.URLEncoder;

    import flash.net.URLRequest;
    import flash.utils.ByteArray;

    public class OAuthSignatureMethod_HMAC_SHA1
        implements IOAuthSignatureMethod
    {
        public function OAuthSignatureMethod_HMAC_SHA1()
        {
        }

        public function get name():String
        {
            return "HMAC-SHA1";
        }

        public function signRequest(urlRequest : URLRequest, signer : OAuthSigner, oauthParameters : OAuthParameters, tokenSecret : String = null) : String
        {
            // get the signable string
            var baseString : String = signer.getSignatureBaseString(urlRequest, oauthParameters);

            // get the secrets to encrypt with
            var sSec:String = URLEncoder.encode(signer.consumerSecret) + "&";
            if (tokenSecret && tokenSecret != OAuthConstants.EMPTY_TOKEN_SECRET)
            {
                sSec += URLEncoder.encode(tokenSecret);
            }

            // hash them
            var hmac:HMAC = Crypto.getHMAC("sha1");
            var key:ByteArray = Hex.toArray(Hex.fromString(sSec));
            var data:ByteArray = Hex.toArray(Hex.fromString(baseString));
            var result:ByteArray = hmac.compute(key, data);

            var ret:String = Base64.encodeByteArray(result);
            return ret;
        }
    }
}