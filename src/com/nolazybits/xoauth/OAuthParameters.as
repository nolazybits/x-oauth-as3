/**
 * User: xavier
 * Date: 7/27/12
 * Time: 2:22 PM
 */
package com.nolazybits.xoauth
{
    import com.nolazybits.xoauth.signaturesMethods.IOAuthSignatureMethod;
    import com.nolazybits.xoauth.signaturesMethods.OAuthSignatureMethod_HMAC_SHA1;
    import com.nolazybits.xoauth.utils.URLEncoder;

    import flash.net.URLVariables;
    import flash.utils.Dictionary;

    public class OAuthParameters
    {
        private var __parameters : Dictionary;

        private static const ALPHA_CHAR_CODES:Array = [48, 49, 50, 51, 52, 53, 54,
                                                       55, 56, 57, 65, 66, 67, 68, 69, 70];

        public function OAuthParameters( consumerKey : String, signatureMethod : IOAuthSignatureMethod, version : String = "1.0") : void
        {
            if( signatureMethod === null )
            {
                signatureMethod = new OAuthSignatureMethod_HMAC_SHA1();
            }

            __parameters = new Dictionary();
            __parameters[OAuthConstants.TIMESTAMP]      = __getTimestampInSeconds();
            __parameters[OAuthConstants.SIGN_METHOD]    = signatureMethod.name;
            __parameters[OAuthConstants.VERSION]        =  version;
            __parameters[OAuthConstants.NONCE]          = __getNonce();
            __parameters[OAuthConstants.CONSUMER_KEY]   = consumerKey;
        }


        /**
         * Add a given parameter.
         * @param key Parameter key.
         * @param value Parameter value.
         */
        public function add(key : String, value : String) : void
        {
            __parameters[key] = value;
        }

        /**
         * Remove a given parameter.
         * @param key Parameter key.
         * @param value Parameter value.
         */
        public function remove(key : String) : void
        {
            delete __parameters[key];
        }

        public function addParameters(requestParameters:Object) : void
        {
            var param : String;
            for (param in requestParameters)
            {
                __parameters[param] = requestParameters[param];
            }
        }

        /**
         * <p>
         * Get the Authorization header value.
         * </p>
         * @return Value.
         */
        public function getAuthorizationHeaderValue() : String
        {
            var headerString    : String = "OAuth ";
            var params          : Array = getSortedParameters();
            var paramsCount     : uint = params.length;
            var param           : Object;

            for (var i : int = 0; i < paramsCount; i++)
            {
                param = params[i] as Object;
            //  if the string starts with the OAuth prefix (oauth_)
                if (param.key.toString().indexOf(OAuthConstants.PARAM_PREFIX) == 0)
                {
                    headerString += URLEncoder.encode(param.key);
                    headerString += '=';
                    headerString += '"';
                    headerString += URLEncoder.encode(param.value);
                    headerString += '", ';
                }
            }

            return headerString.substring(0, headerString.length - 2);
        }

        /**
         * Get a string with all the parameters sorted and URL encoded.
         * @return sorted String
         */
        public function getSortedEncodedParametersAsString() : String
        {
            var params:Array = getSortedParameters();
            var paramsCount     : uint = params.length;
            var param           : Object;
            var encodedString   : String = '';

            for (var i : int = 0; i < paramsCount; i++)
            {
                param = params[i] as Object;
                if (param.key != OAuthConstants.SIGNATURE)
                {
                    encodedString += URLEncoder.encode(param.key);
                    encodedString += '=';
                    encodedString += URLEncoder.encode(param.value);
                    encodedString += '&';
                }
            }

            // return them like a querystring
            return encodedString.substring(0, encodedString.length - 1);
        }

        public function getSortedParameters() : /*Object*/ Array
        {
            var params  : Array = new Array();
            var param   : String;

            // loop over params, find the ones we need
            for ( param in __parameters)
            {
                params.push({key:param, value:__parameters[param].toString()});
            }

            // put them in the right order
            params.sortOn("key");

            return params;
        }

        public function getParameters() : URLVariables
        {
            var urlVariable : URLVariables = new URLVariables();
            var params  : Array = new Array();
            var param   : String;

            // loop over params, find the ones we need
            for ( param in __parameters)
            {
                urlVariable[param] = __parameters[param].toString();
            }

            return urlVariable;
        }

        private function __getTimestampInSeconds() : uint
        {
            return new Date().getTime() / 1000;
        }

        /**
         * Returns a unique id to use a a noonce.
         * Source from https://bitbucket.org/photobucket/api-as3/src/2ef225b9cab3/src/com/photobucket/webapi/oauth/OAuthBaseService.as
         * @return
         */
        public static function __getNonce():String
        {
            var uid2:Array = new Array(36);
            var index:int = 0;
            var i:int;
            var j:int;
            for (i = 0; i < 8; i++)
            {
                uid2[index++] = ALPHA_CHAR_CODES[Math.floor(Math.random() *  16)];
            }

            for (i = 0; i < 3; i++)
            {
                uid2[index++] = 45; // charCode for "-"
                for (j = 0; j < 4; j++)
                {
                    uid2[index++] = ALPHA_CHAR_CODES[Math.floor(Math.random() *  16)];
                }
            }

            uid2[index++] = 45; // charCode for "-"
            var time:Number = new Date().getTime();
            // Note: time is the number of milliseconds since 1970,
            // which is currently more than one trillion.
            // We use the low 8 hex digits of this number in the UID.
            // Just in case the system clock has been reset to
            // Jan 1-4, 1970 (in which case this number could have only
            // 1-7 hex digits), we pad on the left with 7 zeros
            // before taking the low digits.
            var timeString:String = ("0000000" + time.toString(16).toUpperCase()).substr(-8);
            for (i = 0; i < 8; i++)
            {
                uid2[index++] = timeString.charCodeAt(i);
            }
            for (i = 0; i < 4; i++)
            {
                uid2[index++] = ALPHA_CHAR_CODES[Math.floor(Math.random() *  16)];
            }
            return String.fromCharCode.apply(null, uid2);
        }
    }
}
