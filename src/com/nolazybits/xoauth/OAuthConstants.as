/**
 * User: xavier
 * Date: 7/27/12
 * Time: 2:23 PM
 */
package com.nolazybits.xoauth
{
    public class OAuthConstants
    {
        /**
         * Timestamp
         */
        public static const TIMESTAMP           : String = "oauth_timestamp";

        /**
         * Signature Method
         */
        public static const SIGN_METHOD         : String= "oauth_signature_method";

        /**
         * Signature
         */
        public static const SIGNATURE           : String= "oauth_signature";

        /**
         * CONSUMER_KEY.
         */
        public static const CONSUMER_KEY        : String = "oauth_consumer_key";

        /**
         * OAuth version (1.0 or 1.0a)
         */
        public static const VERSION             : String = "oauth_version";

        /**
         * Nonce
         */
        public static const NONCE               : String = "oauth_nonce";

        /**
         * OAuth parameters prefix
         */
        public static const PARAM_PREFIX        : String = "oauth_";

        /**
         * Token
         */
        public static const TOKEN               : String = "oauth_token";

        /**
         * Empty token secret
         */
        public static const EMPTY_TOKEN_SECRET  : String = "";

        /**
         * Header
         */
        public static const HEADER              : String = "Authorization";

        /**
         * xAuth mode
         */
        public static const MODE                : String = "x_auth_mode";

        /**
         * xAuth required username parameter
         */
        public static const USERNAME            : String = "x_auth_username";

        /**
         * xAuth required password parameter
         */
        public static const PASSWORD            : String = "x_auth_password";

        /**
         * Callback URI
         */
        public static const CALLBACK            : String = "oauth_callback";

        /**
         * Verifier
         */
        public static const VERIFIER            : String = "oauth_verifier";
    }
}
